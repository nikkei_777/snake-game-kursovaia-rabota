package objects;

import game.SnakeClassic;

public class Food {
    
    public int posX;
    public int posY;

    public Food(int startX, int startY) {
        posX = startX;
        posY = startY;
    }

    public void position() {
        posX = (int)(Math.random()*SnakeClassic.width);
        posY = (int)(Math.random()*SnakeClassic.height);
    }
}
