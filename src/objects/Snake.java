package objects;

import game.SnakeClassic;

public class Snake {

    public int direction = 0;//Направление змейки
    public static int length = 2;//Начальная длина змейки
    public static int point = 0;
    public static int snakeX[] = new int[100];//max длина змейки
    public static int snakeY[] = new int[100];//max длина змейки

    public Snake(int x0, int y0) {//Стартовая позиция змейки
        snakeX[0] = x0;
        snakeY[0] = y0;
    }

    public static void setPos(int x0, int y0) {
        snakeX[0] = x0;
        snakeY[0] = y0;
        length = 2;
    }

    public void move() {//Движение змейки

        for (int d = length; d > 0; d--) {
            snakeX[d] = snakeX[d - 1];
            snakeY[d] = snakeY[d - 1];
        }

        if (direction == 0) {
            snakeX[0]++;//Движение вправо 
        }
        if (direction == 1) {
            snakeY[0]++;//Движение вниз
        }
        if (direction == 2) {
            snakeX[0]--;//Движение влево
        }
        if (direction == 3) {
            snakeY[0]--;//Движение вверх
        }
    }
}
