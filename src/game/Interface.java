package game;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import objects.Snake;

public class Interface extends JFrame {

    public static JButton butCl, butRel, butMad, butPrav, butRec, butExit, back;
    public static JPanel panelUPrezh, panelDinfo,panelUPzm, panelPicture, panelR;
    public static JFrame jfr1, jfr2, jfrRec;
    public static JTextField tablo;
    public JLabel och = new JLabel("Очки: ");
    public static char[] hist = new char[100];
    public static int rowIndex=0;
    public static String [][] dataH = new String [50][2];  // История игр 
    public static JTable jTable1 = new JTable();
    public String[] columnNames = {
     "Режим",
     "Очки"
    };
    
    Calendar calendar = new GregorianCalendar();
    SimpleDateFormat formattedDate = new SimpleDateFormat("Дата: dd.MM.yyyy |");
    SimpleDateFormat formattedTime = new SimpleDateFormat("Время запуска игры: HH:mm:ss");
    String dateToday = formattedDate.format(calendar.getTime());
    String timeToday = formattedTime.format(calendar.getTime());
    JLabel date = new JLabel(dateToday);
    JLabel time = new JLabel(timeToday);
    JLabel datezm = new JLabel(dateToday);
    JLabel timezm = new JLabel(timeToday);

    public void Frame() {

        jfr1 = new JFrame("Snake Game");//Создание окна
        jfrRec = new JFrame("Статистика");//Создание окна
        panelUPrezh = new JPanel();
        panelDinfo = new JPanel();
        panelUPzm = new JPanel();
        panelPicture = new JPanel();
        panelR = new JPanel();
        panelUPrezh.setBorder(new TitledBorder("Режим игры"));
        panelDinfo.setBorder(new TitledBorder("Об игре"));
        panelUPzm.setBorder(new TitledBorder("Информация"));

        jfr1.setSize(600, 590);//Размерность
        jfr1.setLocationRelativeTo(null);//Расположение окошка по центру
        jfr1.setResizable(false);//Нельзя менять размеры окна
        jfr1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Для остановки сборки по нажатию кнопки

        butCl = new JButton("Классика");
        butRel = new JButton("Релакс");
        butMad = new JButton("Безумие");
        butPrav = new JButton("Правила");
        butRec = new JButton("Статистика игр");
        butExit = new JButton("Выход");
        back = new JButton("Назад");

        panelUPrezh.setLayout(new GridLayout(1, 3, 1, 1));
        panelUPrezh.add(butCl);
        panelUPrezh.add(butRel);
        panelUPrezh.add(butMad);

        panelDinfo.setLayout(new GridLayout(3, 1, 1, 1));
        panelDinfo.add(butPrav);
        panelDinfo.add(butRec);
        panelDinfo.add(butExit);

        panelPicture.setLayout(new FlowLayout());
        JLabel pictSnake = new JLabel(new ImageIcon("snake.jpg"));
        panelPicture.add(pictSnake);

        panelUPzm.setLayout(new FlowLayout(0));
        tablo = new JTextField(2); 
        tablo.setText(Integer.toString(Snake.point));
        tablo.setEditable(false);
        panelUPzm.add(och);
        panelUPzm.add(tablo);

        
        panelUPzm.add(datezm);
        panelUPzm.add(timezm);

        jfr1.getContentPane().add(BorderLayout.NORTH, panelUPrezh);
        jfr1.getContentPane().add(BorderLayout.CENTER, panelPicture);
        jfr1.getContentPane().add(BorderLayout.SOUTH, panelDinfo);

        jfr1.setVisible(true);
        setListeners();
    }

    public static void settext(){
    tablo.setText(Integer.toString(Snake.point));
    }
    
    //Назначение кнопок
    public void setListeners() {
        butCl.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Snake.point = 0; 
                Interface.settext();               
                SnakeClassic.gameOver = false;
                jfr2 = new JFrame("Snake Game Режим: Классика");
                jfr2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Для остановки сборки по нажатию кнопки
                jfr2.setResizable(false);//Нельзя менять размеры окна
                jfr2.setSize(GlobalRezhim.width * GlobalRezhim.scale + 7, GlobalRezhim.height * GlobalRezhim.scale + 82);//Размерность окна
                jfr2.setLocationRelativeTo(null);//Расположение окошка по центру
                jfr2.add(SnakeClassic.sg);//В JFrame добавляем игровую область
                jfr2.getContentPane().add(BorderLayout.NORTH, panelUPzm);
                jfr2.setVisible(true);
                jfr1.setVisible(false);
                JOptionPane.showMessageDialog(null, "Для начала игры нажмите клавишу ПРОБЕЛ", "Начать игру", JOptionPane.WARNING_MESSAGE);
                SnakeClassic.stop = false;
                SnakeClassic.sg.grabFocus();
            }
        });

        butRel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Snake.point = 0;
                Interface.settext();
                SnakeRelax.gameOver = false;
                jfr2 = new JFrame("Snake Game Режим: Релакс");
                jfr2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Для остановки сборки по нажатию кнопки
                jfr2.setResizable(false);//Нельзя менять размеры окна
                jfr2.setSize(GlobalRezhim.width * GlobalRezhim.scale + 7, GlobalRezhim.height * GlobalRezhim.scale + 82);//Размерность окна
                jfr2.setLocationRelativeTo(null);//Расположение окошка по центру
                jfr2.add(SnakeRelax.sg);//В JFrame добавляем игровую область
                jfr2.getContentPane().add(BorderLayout.NORTH, panelUPzm);
                jfr2.setVisible(true);
                jfr1.setVisible(false);
                JOptionPane.showMessageDialog(null, "Для начала игры нажмите клавишу ПРОБЕЛ", "Начать игру", JOptionPane.WARNING_MESSAGE);
                SnakeRelax.stop = false;
                SnakeRelax.sg.grabFocus();
            }
        });

        butMad.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Snake.point = 0;
                Interface.settext();
                SnakeMadness.gameOver = false;
                jfr2 = new JFrame("Snake Game Режим: Безумие");
                jfr2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Для остановки сборки по нажатию кнопки
                jfr2.setResizable(false);//Нельзя менять размеры окна
                jfr2.setSize(GlobalRezhim.width * GlobalRezhim.scale + 7, GlobalRezhim.height * GlobalRezhim.scale + 82);//Размерность окна
                jfr2.setLocationRelativeTo(null);//Расположение окошка по центру
                jfr2.add(SnakeMadness.sg);//В JFrame добавляем игровую область
                jfr2.getContentPane().add(BorderLayout.NORTH, panelUPzm);
                jfr2.setVisible(true);
                jfr1.setVisible(false);
                JOptionPane.showMessageDialog(null, "Для начала игры нажмите клавишу ПРОБЕЛ", "Начать игру", JOptionPane.WARNING_MESSAGE);
                SnakeMadness.stop = false;
                SnakeMadness.sg.grabFocus();
            }
        });

        butPrav.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Режим 'Классика' - избегайте столкновения со стенками и с телом, скорость змейки увеличивается с каждым съеденым яблоком .\n"
                        + "\nРежим 'Релакс' - Змейка проезжает через стены, избегайте столкновения с телом, скорость змейки постоянна.\n "
                        + "\nРежим 'Безумие' - каждый шаг змейки меняет цветовое изображение игры, избегайте столкновения с телом. "
                        + "\nКнопки управления инвертированы (При нажатии ↑ - змейка движется вниз, → - влево, ↓ - вверх, ← - вправо.)");
            }
        });
        
        butRec.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {     
                panelR.add(back);
                jfrRec.getContentPane().add(BorderLayout.SOUTH,panelR);
                jfrRec.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //close the window 
                
                 try
                {
                    FileReader file = new FileReader("data.txt");
                    file.read(hist);
                } catch (IOException ex) {                 
                }
                int k=0;
                rowIndex=0;
                
                for (int i=0; i<hist.length; i++)  // Расфасовывание файла по переменным
                {
                    if(hist[i]=='.')
                    {
                        if (hist[i-1]=='1'){
                            dataH[rowIndex][k]="Классика";
                        }
                        if (hist[i-1]=='2'){
                            dataH[rowIndex][k]="Релакс";
                        }
                        if (hist[i-1]=='3'){
                            dataH[rowIndex][k]="Безумие";
                        }
                        k=1;
                    }
                        
                    if(hist[i]=='>')
                    {
                        dataH[rowIndex][k]=""+hist[i-1];
                        k=0;                       
                        rowIndex++;
                    }                                
                }
                
                jTable1 = new JTable(dataH, columnNames);
                JScrollPane scrollPane = new JScrollPane(jTable1);
                jfrRec.getContentPane().add(scrollPane);
                jTable1.setVisible(true);
                
                jfrRec.setSize(300, 400);
                jfr1.setVisible(false);
                jfrRec.setVisible(true);              
                jfrRec.setLocationRelativeTo(null);//Расположение окошка по центру
                jfrRec.setResizable(false);//Нельзя менять размеры окна
            }
        });
        
        back.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                jfr1.setVisible(true);
                jfrRec.setVisible(false);                
            }
        });
        
        butExit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (JOptionPane.showConfirmDialog(null, "Вы уверены, что хотите выйти из игры?") == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
    }

}
