package game;

import static game.Interface.jfr1;
import static game.Interface.jfr2;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import objects.Snake;
import static objects.Snake.length;
import static objects.Snake.snakeX;
import static objects.Snake.snakeY;

public class SnakeRelax extends GlobalRezhim {

    private static int speed = 15;//Скорость змейки (клеток в секунду)
    public static boolean stop = true;
    public static boolean gameOver = false;
    private Timer t = new Timer(1000 / speed, this);

    public SnakeRelax() {
        t.start();
        addKeyListener(new KeyBoard());
        setFocusable(true);
    }

    static SnakeRelax sg = new SnakeRelax();

    @Override
    public void paint(Graphics g) {//Отрисовка графики
        g.setColor(myColor(135, 206, 255));
        g.fillRect(0, 0, width * scale, height * scale);//Область закрашивания
        g.setColor(myColor(122, 197, 205));//Установка цвета 

        for (int xx = 0; xx <= width * scale; xx += scale) {//Отрисовка вертикальных линий в окне
            g.drawLine(xx, 0, xx, width * scale);
        }
        for (int yy = 0; yy <= height * scale; yy += scale) {//Отрисовка горизонтальных линий в окне
            g.drawLine(0, yy, width * scale, yy);
        }

        for (int d = 0; d < Snake.length; d++) {
            g.setColor(myColor(255, 52, 179));
            g.fillRect(Snake.snakeX[d] * scale + 1, Snake.snakeY[d] * scale + 1, scale - 1, scale - 1);
        }

        g.setColor(myColor(255, 255, 0));
        g.fillRect(f.posX * scale + 1, f.posY * scale + 1, scale - 1, scale - 1);
    }

    public class KeyBoard extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent kEvt) {
            int key = kEvt.getKeyCode();//Получение кода нажатой клавиши

            if ((key == KeyEvent.VK_RIGHT) && s.direction != 2) {
                s.direction = 0;
            }
            if ((key == KeyEvent.VK_DOWN) && s.direction != 3) {
                s.direction = 1;
            }
            if ((key == KeyEvent.VK_LEFT) && s.direction != 0) {
                s.direction = 2;
            }
            if ((key == KeyEvent.VK_UP) && s.direction != 1) {
                s.direction = 3;
            }
            if ((key == KeyEvent.VK_SPACE) && (gameOver != true)) {
                t.start();
            }
        }
    }

    private void gOver() {
        speed = 15;
        JOptionPane.showMessageDialog(null, "Игра в режиме 'релакс' окончена, вы проиграли!", "Game Over", JOptionPane.WARNING_MESSAGE);
        gameOver = true;
        Snake.setPos(15, 15);
        t.stop();
        jfr2.dispose();
        jfr1.setVisible(true);
        theEnd();
    }

    private static void theEnd() // метод результата игры
    {
        try (FileWriter file = new FileWriter("data.txt", true)) {
            file.write(2 + "." + Snake.point + ">");
            file.close();
        } catch (Exception e) {
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        s.move();
        if ((Snake.snakeX[0] == f.posX) && (Snake.snakeY[0] == f.posY)) {//Если головной элемент совпадает с коорд. еды
            f.position();//Генерируется новое местоположение еды
            Snake.length++;//Длина змейки увеличивается
            Snake.point++;//Очки увеличиваются   
            Interface.settext();
        }

        for (int k = 1; k < Snake.length; k++) {
            if ((Snake.snakeX[k] == f.posX) && (Snake.snakeY[k] == f.posY)) {//Если элемент тела змейки будет совпадать с коорд. еды, то
                f.position();//Генерируется новое местоположение еды
            }
        }

        for (int d = Snake.length; d > 0; d--) {//Змейка съедает свой хвост
            if ((Snake.snakeX[0] == Snake.snakeX[d]) && (Snake.snakeY[0] == Snake.snakeY[d])) {
                gOver();
            }
        }

        if (snakeX[0] > SnakeRelax.width - 1) {
            snakeX[0] = 0;
        }
        if (snakeX[0] < 0) {
            snakeX[0] = SnakeRelax.width - 1;
        }
        if (snakeY[0] > SnakeRelax.height - 1) {
            snakeY[0] = 0;
        }
        if (snakeY[0] < 0) {
            snakeY[0] = SnakeRelax.height - 1;
        }
        if (length < 2) {
            length = 2;
        }

        if (stop == true) {
            t.stop();
        }
        repaint();//Перерисовка

    }
}
