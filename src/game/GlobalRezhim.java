package game;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JPanel;
import objects.Food;
import objects.Snake;

public class GlobalRezhim extends JPanel implements ActionListener{

    public static final int scale = 20;//Размерность одной клетки
    public static final int width = 30;//Количество клеток по ширине
    public static final int height = 30;//Количество клеток по высоте
    public final Random rand = new Random();

    protected Food f = new Food((int) (Math.random() * width), (int) (Math.random() * height));//Устанавливается начальное знач. еды
    protected Snake s = new Snake(15, 15);//Устанавливаем нач. положение змейки


    protected Color myColor(int r, int g, int b) {//Метод для создания своих цветов
        return new Color(r, g, b);
    }

    protected Color rColor() {//Метод для создания рандомных цветов
        int r = rand.nextInt(255);
        int g = rand.nextInt(255);
        int b = rand.nextInt(255);
        return new Color(r, g, b);
    }

    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
