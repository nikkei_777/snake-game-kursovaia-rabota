package game;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sounds {

    private static Clip clip = null;

    public Sounds(File f) {
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(f);
            clip = AudioSystem.getClip();
            clip.open(stream);
        } catch (IOException | UnsupportedAudioFileException | LineUnavailableException exc) {
        }
    }

    public static Sounds playSound(String s) {
        File f = new File(s);
        Sounds snd = new Sounds(f);
        clip.loop(100);
        return snd;
    }
}
